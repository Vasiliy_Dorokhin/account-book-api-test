package com.test.teststeps;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.adapter.HttpAdapter;
import com.test.variables.RestConstants;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RestSteps {
    @NonNull
    private HttpAdapter httpAdapter;
    private Gson jsonConverter = new GsonBuilder().create();

    public <T> T sendRequest(RestConstants.RequestMethod requestMethod, RestConstants.ContentType contentType, String endPoint, String request, Class<T> type) {
        HttpAdapter.Response response = httpAdapter
                .sendRequest(requestMethod, contentType, endPoint, request);
        return jsonConverter.fromJson(response.getBody(), type);
    }

    public String sendRequest(RestConstants.RequestMethod requestMethod, RestConstants.ContentType contentType,
                              String endPoint, String requestBody, RestConstants.ResponseCode expectedResponseCode) {
        HttpAdapter.Response response = httpAdapter
                .sendRequest(requestMethod, contentType, endPoint, requestBody)
                .expectCode(expectedResponseCode);
        return response.getBody();
    }

    public HttpAdapter.Response sendRequest(RestConstants.RequestMethod requestMethod, RestConstants.ContentType contentType,
                                            String endPoint, String request) {
        return httpAdapter.sendRequest(requestMethod, contentType, endPoint, request);
    }

}

