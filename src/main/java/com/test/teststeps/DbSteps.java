package com.test.teststeps;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.adapter.HttpAdapter;
import com.test.model.Entity;
import com.test.model.Expense;
import com.test.variables.EndPoint;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.test.variables.RestConstants.ContentType.JSON;
import static com.test.variables.RestConstants.RequestMethod.DELETE;
import static com.test.variables.RestConstants.RequestMethod.GET;
import static com.test.variables.RestConstants.RequestMethod.POST;

@Slf4j
@RequiredArgsConstructor
public class DbSteps {
    @NonNull
    private HttpAdapter httpAdapter;
    private Gson jsonConverter = new GsonBuilder().create();

    public Expense getExpenseFromDb(String id) {
        Map<String, Expense> expensesFromDb = getExpensesFromDb();
        Expense expenseFromDb = expensesFromDb.get(id);
        if (expenseFromDb != null) {
            log.info("Entity from DB - [{}]", expenseFromDb.toString());
        }
        return expenseFromDb;
    }

    public Map<String, Expense> getExpensesFromDb() {
        Map<String, Expense> map = new HashMap<>();
        HttpAdapter.Response entitiesFromDb = httpAdapter.sendRequest(GET, JSON, EndPoint.EXPENSE.getValue(), null);
        Entity[] entities = jsonConverter.fromJson(entitiesFromDb.getBody(), Entity[].class);
        Arrays.asList(entities).forEach(b -> map.put(String.valueOf(b.getId()), b.getExpense()));
        return map;
    }

    public void generateTestValuesInDb() {
        List<Expense> expenses = ImmutableList.<Expense>builder()
                .add(Expense.builder()
                        .amount(0.1)
                        .category(Expense.ExpenseCategory.FOOD)
                        .comment("test value 1")
                        .build())
                .add(Expense.builder()
                        .amount(1.0)
                        .category(Expense.ExpenseCategory.HOME)
                        .comment("test value 2")
                        .build())
                .add(Expense.builder()
                        .amount(1.1)
                        .category(Expense.ExpenseCategory.TRANSPORT)
                        .comment("test value 3")
                        .build())
                .add(Expense.builder()
                        .amount(10.0)
                        .category(Expense.ExpenseCategory.ENTERTAINMENTS)
                        .comment("test value 4")
                        .build())
                .add(Expense.builder()
                        .amount(10.1)
                        .category(Expense.ExpenseCategory.MEDICINE)
                        .comment("test value 5")
                        .build())
                .add(Expense.builder()
                        .amount(11.0)
                        .category(Expense.ExpenseCategory.TRAVEL)
                        .comment("test value 6")
                        .build())
                .add(Expense.builder()
                        .amount(11.1)
                        .category(Expense.ExpenseCategory.OTHER)
                        .comment("test value 7")
                        .build())
                .build();
        expenses.forEach(b -> httpAdapter.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue(), jsonConverter.toJson(b)));
    }

    public void cleanUpDb() {
        httpAdapter.sendRequest(DELETE, JSON, EndPoint.EXPENSE.getValue(), null);
    }
}
