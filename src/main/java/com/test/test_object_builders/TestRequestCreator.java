package com.test.test_object_builders;

import com.test.model.Expense;

/**
 * Класс для создания запросов c различными типами полей в тестах
 */
public class TestRequestCreator {
    private static final String REQUEST_FORMAT = "{\"expense_amount\":%s,\"expense_category\":\"%s\",\"expense_comment\":\"%s\"}";

    public static String create(String comment) {
        return create(42.5, Expense.ExpenseCategory.FOOD.name(), comment);
    }

    public static String create(Number amount) {
        return create(amount, Expense.ExpenseCategory.FOOD.name(), amount);
    }

    public static String create(String category, String comment) {
        return create(42.5, category, comment);
    }

    public static String create(Object amount, Object category, Object comment) {
        return String.format(REQUEST_FORMAT,String.valueOf(amount), String.valueOf(category), String.valueOf(comment));
    }
}
