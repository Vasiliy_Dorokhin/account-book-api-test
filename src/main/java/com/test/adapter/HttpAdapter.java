package com.test.adapter;

import com.test.variables.RestConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Cleanup;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Вообще, когда нужен клиент, в последнее время, использую Retrofit2.
 * Но в тестах не могу себя заставить его использовать. :)
 * Да, круто описывать API сервиса в одном месте как интерфейс.
 * Но в процессе появляется слишком много лишних абстракций
 * и относительно жёсткая завязка на объект получаемых данных и пр.
 * А хочется иметь возможность изменить и добавить любой аспект запроса.
 * Тут нет аутентификации, хедеров и пр., только то что используется в тестах.
 *
 * P.S. Если есть предложения как это ещё можно реализовать, с радостью послушаю.
 */
@Slf4j
public class HttpAdapter {
    private static final int connectTimeout = 100;

    private URL url;

    @Builder
    public HttpAdapter(@NotNull String uri, Integer port, String basePath) {
        String urlString = uri;
        if (port != null) {
            urlString = urlString.concat(":" + port);
        }
        if (basePath != null) {
            urlString = urlString.concat(basePath);
        }
        try {
            url = new URL(urlString);
        } catch (Exception e) {
            fail(String.format("Can't get URL %s. Reason: %s", urlString, e.getLocalizedMessage()));
        }
    }

    public Response sendRequest(RestConstants.RequestMethod requestMethod, RestConstants.ContentType contentType, String endPoint,
                                String requestData) {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = createConnection(endPoint);
            httpURLConnection.setRequestProperty("Content-Type", contentType.getValue());
            httpURLConnection.setRequestMethod(requestMethod.getValue());
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            if (requestData != null) {
                setRequestDataToHttpUrlConnection(httpURLConnection, requestData);
            }

        } catch (Exception e) {
            log.error("Can't send request. Reason: {}", e.getLocalizedMessage());
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
                log.info("Request method: {}\nRequest URI: {}\nParameters: {}\nHeaders: {}",
                        httpURLConnection.getRequestMethod(), httpURLConnection.getURL(), requestData, httpURLConnection.getRequestProperties());
            }
        }

        return new Response(getResponseCode(httpURLConnection), getResponseBody(httpURLConnection));
    }

    private void setRequestDataToHttpUrlConnection(HttpURLConnection httpURLConnection, String requestData) throws IOException {
        byte[] paramsData = requestData.getBytes(UTF_8);
        int paramsLength = paramsData.length;
        httpURLConnection.setRequestProperty("charset", "utf-8");
        httpURLConnection.setRequestProperty("Content-Length", Integer.toString(paramsLength));
        httpURLConnection.setUseCaches(false);
        @Cleanup("flush") DataOutputStream out = new DataOutputStream(httpURLConnection.getOutputStream());
        out.write(paramsData);
    }

    private int getResponseCode(HttpURLConnection httpURLConnection) {
        try {
            int responseCode = httpURLConnection.getResponseCode();
            log.info("Response code is: {}", responseCode);
            return responseCode;
        } catch (Exception e) {
            log.error("Can't get response code. Reason: {}", e.getLocalizedMessage());
            return -1;
        }
    }

    @Nullable
    private String getResponseBody(HttpURLConnection httpURLConnection) {
        InputStream inputStream;
        try {
            inputStream = httpURLConnection.getInputStream();
        } catch (Exception e) {
            inputStream = httpURLConnection.getErrorStream();
        }
        if (inputStream == null) {
            return null;
        }
        try {
            @Cleanup BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, UTF_8));
            String responseBody = reader.lines().collect(Collectors.toList()).get(0);
            log.info("Response body: {}", responseBody);
            return responseBody;
        } catch (Exception e) {
            log.error("Can't close buffer reader. Reason: {}", e.getLocalizedMessage());
            return null;
        }

    }

    private HttpURLConnection createConnection(String endPoint) throws IOException {
        URL urlWithEndPoint = new URL(url.toString().concat(endPoint));

        HttpURLConnection httpURLConnection = (HttpURLConnection) urlWithEndPoint.openConnection();
        HttpURLConnection.setFollowRedirects(true);
        httpURLConnection.setConnectTimeout(connectTimeout);

        httpURLConnection.setRequestProperty("Accept", "*/*");

        return httpURLConnection;
    }

    @Getter
    @AllArgsConstructor
    public class Response {
        private int code;
        private String body;

        public boolean responseCodeIsExpected(RestConstants.ResponseCode expectedResponseCode) {
            if (code == expectedResponseCode.getValue()) {
                return true;
            }
            log.info("Expected response code is {}", expectedResponseCode.getValue());
            return false;
        }

        public Response expectCode(RestConstants.ResponseCode expectedResponseCode) {
            assertEquals(expectedResponseCode.getValue(), code,
                    "Wrong code");
            return this;
        }

        public boolean responseBodyIsExpected(String expectedBody) {
            if (Objects.equals(body, expectedBody)) {
                return true;
            }
            log.info("Expected response body is {}", expectedBody);
            return false;
        }
    }
}

