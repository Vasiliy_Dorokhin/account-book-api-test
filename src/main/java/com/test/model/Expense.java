package com.test.model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@AllArgsConstructor
@Builder(toBuilder = true)
public class Expense {
    @SerializedName(value="expense_amount")
    @Builder.Default Double amount = 213.5;
    @SerializedName(value="expense_category")
    @Builder.Default ExpenseCategory category = ExpenseCategory.TRANSPORT;
    @SerializedName(value="expense_comment")
    @Builder.Default String comment = "Road on work.";

    public enum ExpenseCategory {
        FOOD,
        HOME,
        TRANSPORT,
        ENTERTAINMENTS,
        MEDICINE,
        TRAVEL,
        OTHER,
        UNKNOWN
    }
}
