package com.test.variables;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum EndPoint {
    INFO("/info"),
    EXPENSE("/expense");

    @Getter
    private String value;
}
