package com.test.variables;

public class DockerConstants {
    public static final int DEFAULT_HTTP_PORT = 8080;

    public static final String ACCOUNT_BOOK_IMAGE_NAME = "dorohin04/account-book-test-service:latest";
}
