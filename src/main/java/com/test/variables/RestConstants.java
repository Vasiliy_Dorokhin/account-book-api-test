package com.test.variables;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class RestConstants {
    @AllArgsConstructor
    public enum RequestMethod {
        GET("GET"),
        POST("POST"),
        PUT("PUT"),
        DELETE("DELETE");

        @Getter
        private String value;
    }

    @AllArgsConstructor
    public enum ContentType {
        JSON ("application/json");

        @Getter
        private String value;
    }

    @AllArgsConstructor
    public enum ResponseCode {
        OK(200),
        CREATED(201),
        ACCEPTED(202),
        NON_AUTHORITATIVE_INFORMATION(203),
        BAD_REQUEST(400),
        NOT_FOUND(404),
        METHOD_NOT_ALLOWED(405),
        TEAPOT(418), // Почему бы и нет :)
        INTERNAL_SERVER_ERROR(500);

        @Getter
        private int value;
    }

}