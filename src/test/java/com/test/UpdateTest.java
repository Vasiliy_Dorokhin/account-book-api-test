package com.test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.adapter.HttpAdapter;
import com.test.model.Expense;
import com.test.teststeps.DbSteps;
import com.test.teststeps.RestSteps;
import com.test.variables.EndPoint;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Map;
import java.util.stream.Stream;

import static com.test.variables.DockerConstants.ACCOUNT_BOOK_IMAGE_NAME;
import static com.test.variables.DockerConstants.DEFAULT_HTTP_PORT;
import static com.test.variables.RestConstants.ContentType.JSON;
import static com.test.variables.RestConstants.RequestMethod.PUT;
import static com.test.variables.RestConstants.ResponseCode.INTERNAL_SERVER_ERROR;
import static com.test.variables.RestConstants.ResponseCode.OK;

@Slf4j
@Testcontainers
public class UpdateTest {
    private static RestSteps restSteps;
    private static DbSteps dbSteps;
    private Gson jsonConverter = new GsonBuilder().create();

    @Container
    private static final GenericContainer account_book =
            new GenericContainer(ACCOUNT_BOOK_IMAGE_NAME)
                    .withExposedPorts(DEFAULT_HTTP_PORT)
                    .withLogConsumer(new Slf4jLogConsumer(log));

    @BeforeAll
    static void beforeAll() {
        String BOOK_HOST = account_book.getContainerIpAddress();
        Integer BOOK_PORT = account_book.getMappedPort(DEFAULT_HTTP_PORT);
        HttpAdapter httpAdapter = HttpAdapter.builder()
                .uri("http://" + BOOK_HOST)
                .port(BOOK_PORT)
                .build();
        restSteps = new RestSteps(httpAdapter);
        dbSteps = new DbSteps(httpAdapter);
        dbSteps.generateTestValuesInDb();
    }

    @ParameterizedTest
    @MethodSource("putCurrentEntityRequestArguments")
    @DisplayName("Проверка обновления сущности в БД")
    void putCurrentEntityRequest(String id , Expense expenseFromDb) {
        // Подготавливаем проверочные данные
        Map<String, Expense> entitiesFromDb = dbSteps.getExpensesFromDb();
        int expectedDbSize = entitiesFromDb.size();
        Expense updatedExpense = expenseFromDb.toBuilder()
                .amount(99.0)
                .category(Expense.ExpenseCategory.ENTERTAINMENTS)
                .comment("updateTestComment")
                .build();

        // Выполняем запрос
        // TODO: Тут можно обсудить использование 202(ACCEPTED)
        String idPath = "/" + id;
        String response = restSteps.sendRequest(PUT, JSON, EndPoint.EXPENSE.getValue() + idPath ,
                jsonConverter.toJson(updatedExpense), OK);

        // Проверка
        // TODO На данный момент не обнавляются поля expense_amount и expense_category.
        // TODO Так же не обновляет сущности с типом UNKNOWN, который проставляется при создании сущности типа MEDICINE и бла-бла-бла
        Assertions.assertEquals("OK", response, "Должно быть ОК");
        Assertions.assertEquals(expectedDbSize, dbSteps.getExpensesFromDb().size(),
                "Кол-во сущностей в БД не должно измениться");
        expenseCheck(updatedExpense, dbSteps.getExpenseFromDb(id));
    }

    @Test
    @DisplayName("Проверка обновления несуществующих сущности из БД")
    void putMissingIdRequest() {
        // Подготавливаем проверочные данные
        String idPath = "/213";
        Map<String, Expense> entitiesFromDb = dbSteps.getExpensesFromDb();
        int expectedDbSize = entitiesFromDb.size();
        Expense updatedExpense = Expense.builder()
                .amount(99.0)
                .category(Expense.ExpenseCategory.ENTERTAINMENTS)
                .comment("updateTestComment")
                .build();

        // Отправляем запрос
        // TODO Логично ожидать 404 и какое-нибудь сообщение, но получаем 500 и NPE.
        // Поменял ожидаемый код, так как желательно проверить и сообщение
//        String response = restSteps.sendRequest(PUT, JSON, EndPoint.EXPENSE.getValue() + "/" + idPath , jsonConverter.toJson(updatedExpense), NOT_FOUND);
        String response = restSteps.sendRequest(PUT, JSON, EndPoint.EXPENSE.getValue() + "/" + idPath ,
                jsonConverter.toJson(updatedExpense), INTERNAL_SERVER_ERROR);

        // Проверка
        Assertions.assertEquals("<html><body><h2>500 Internal Server Error</h2></body></html>", response,
                "Здесь должно быть норм сообщение");
        Assertions.assertEquals(expectedDbSize, dbSteps.getExpensesFromDb().size(),
                "Кол-во сущностей в БД не должно измениться");
    }

    private void expenseCheck(Expense expectedExpense, Expense actualExpense) {
        Assertions.assertEquals(expectedExpense.getAmount(), actualExpense.getAmount(),
                "Сумма должна совпадать с указанной при создании");
        Assertions.assertEquals(expectedExpense.getCategory(), actualExpense.getCategory(),
                "Категория должна совпадать с указанной при создании");
        Assertions.assertEquals(expectedExpense.getComment(), actualExpense.getComment(),
                "Коментарий должен совпадать с указанным при создании");
    }

     // Если честно, уже лень писать нормальный метод. Пусть будет хардкод.
    private static Stream<Arguments> putCurrentEntityRequestArguments() {
        Map<String, Expense> entitiesFromDb = dbSteps.getExpensesFromDb();
        return Stream.of(
                Arguments.of("1", entitiesFromDb.get("1")),
                Arguments.of("2", entitiesFromDb.get("2")),
                Arguments.of("3", entitiesFromDb.get("3")),
                Arguments.of("4", entitiesFromDb.get("4")),
                Arguments.of("5", entitiesFromDb.get("5")),
                Arguments.of("6", entitiesFromDb.get("6")),
                Arguments.of("7", entitiesFromDb.get("7"))
        );
    }
}
