package com.test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.adapter.HttpAdapter;
import com.test.model.Entity;
import com.test.model.Expense;
import com.test.teststeps.DbSteps;
import com.test.teststeps.RestSteps;
import com.test.variables.EndPoint;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Stream;

import static com.test.model.Expense.ExpenseCategory.ENTERTAINMENTS;
import static com.test.model.Expense.ExpenseCategory.FOOD;
import static com.test.model.Expense.ExpenseCategory.HOME;
import static com.test.model.Expense.ExpenseCategory.MEDICINE;
import static com.test.model.Expense.ExpenseCategory.OTHER;
import static com.test.model.Expense.ExpenseCategory.TRANSPORT;
import static com.test.model.Expense.ExpenseCategory.TRAVEL;
import static com.test.model.Expense.ExpenseCategory.UNKNOWN;
import static com.test.variables.DockerConstants.ACCOUNT_BOOK_IMAGE_NAME;
import static com.test.variables.DockerConstants.DEFAULT_HTTP_PORT;
import static com.test.variables.RestConstants.ContentType.JSON;
import static com.test.variables.RestConstants.RequestMethod.GET;
import static com.test.variables.RestConstants.ResponseCode.*;


@Slf4j
@Testcontainers
public class ReadTest {
    private static RestSteps restSteps;
    private static DbSteps dbSteps;
    private static final Gson jsonConverter = new GsonBuilder().create();

    @Container
    private static final GenericContainer account_book =
            new GenericContainer(ACCOUNT_BOOK_IMAGE_NAME)
                    .withExposedPorts(DEFAULT_HTTP_PORT)
                    .withLogConsumer(new Slf4jLogConsumer(log));

    @BeforeAll
    static void beforeAll() {
        String BOOK_HOST = account_book.getContainerIpAddress();
        Integer BOOK_PORT = account_book.getMappedPort(DEFAULT_HTTP_PORT);
        HttpAdapter httpAdapter = HttpAdapter.builder()
                .uri("http://" + BOOK_HOST)
                .port(BOOK_PORT)
                .build();
        restSteps = new RestSteps(httpAdapter);
        dbSteps = new DbSteps(httpAdapter);
        dbSteps.generateTestValuesInDb();
    }

    @Test
    @DisplayName("Проверка возврата всех сущностей из БД")
    void getAllEntitiesRequest() {
        // Выполняем запрос
        HttpAdapter.Response response = restSteps.sendRequest(GET, JSON, EndPoint.EXPENSE.getValue(), null);

        // Проверка
        response.responseCodeIsExpected(OK);
        Entity[] actualEntities = jsonConverter.fromJson(response.getBody(), Entity[].class);
        Assertions.assertEquals(dbSteps.getExpensesFromDb().size(), actualEntities.length,
                "Кол-во сущностей в БД и ответе на запрос должно совпадать");
    }

    @Test
    @DisplayName("Проверка возврата конкретной сущности из БД")
    void getCurrentEntityRequest() {
        // Подготавливаем проверочные данные
        Map<String, Expense> entitiesFromDb = dbSteps.getExpensesFromDb();

        // Выполняем запрос и проверяем ответ
        entitiesFromDb.forEach((key, expectedExpense) -> {
            String idPath = "/" + key;
            Entity actualEntity = restSteps.sendRequest(GET, JSON,
                    EndPoint.EXPENSE.getValue() + idPath, null, Entity.class);
            // TODO Не возвращает сущности с типом UNKNOWN, который проставляется при создании записи с категорией MEDICINE
            //  Ответ не в формате json, просто текст null.
            Assertions.assertEquals(expectedExpense, actualEntity.getExpense(),
                    "Сущности из БД и запроса должны совпадать");
        });
    }

    @Test
    @DisplayName("Проверка обработки запроса по сущности отсутствующей в БД")
    void getMissingIdRequest() {
        // Подготавливаем проверочные данные
        String id = "Test";

        // Отправляем запрос
        // TODO Логично ожидать 404 и какое-нибудь сообщение, но получаем 200 и текст - EMPTY. Не в формате json.
        // Поменял ожидаемый код, так как желательно проверить и сообщение
//        String response = restSteps.sendRequest(GET, JSON, EndPoint.EXPENSE.getValue() + "/" + id , null, NOT_FOUND);
        String response = restSteps.sendRequest(GET, JSON,
                EndPoint.EXPENSE.getValue() + "/" + id, null, OK);

        // Проверка
        Assertions.assertEquals("{}", response,
                "Такой себе вариант, отвечать ОК и возвращать даже не json :)");
    }

    @ParameterizedTest(name = "[{index}] expense_category - [{0}]")
    @MethodSource("getAllEntitiesWithSpecificCategoryArguments")
    @DisplayName("Проверка обработки запроса с параметром category")
    void getAllEntitiesWithSpecificCategory(String category) {
        // Подготавливаем запрос
        String query = "?category=" + category;

        // Отправляем запрос
        HttpAdapter.Response response = restSteps.sendRequest(GET, JSON,
                EndPoint.EXPENSE.getValue() + query, null);

        // Проверка
        // Тут мы в очередной раз понимаем, что траты по категории MEDICINE не создаются
        // И за одно проверяем зависимость от регистра
        response.responseCodeIsExpected(OK);
        Entity[] actualEntities = jsonConverter.fromJson(response.getBody(), Entity[].class);
        Assertions.assertEquals(1, actualEntities.length,
                "Должна вернуться только одна сущность");
        Assertions.assertEquals(Expense.ExpenseCategory.valueOf(category.toUpperCase()),
                Arrays.asList(actualEntities).get(0).getExpense().getCategory(),
                "Категория из запроса и категория из сущности должны совпадать");
    }

    @Test
    @DisplayName("Проверка обработки запроса с несколькими значением category")
    void getWithSeveralCategories() {
        // Подготавливаем запрос
        String category = "food,HOME";
        String query = "?category=" + category;

        // Отправляем запрос
        String response = restSteps.sendRequest(GET, JSON,
                EndPoint.EXPENSE.getValue() + query, null, TEAPOT);

        // Проверка
        Assertions.assertEquals("Work with several categories is not supported.", response);
    }


    @Test
    @DisplayName("Проверка обработки запроса с ошибочным значением category")
    void getWithWrongCategory() {
        // Подготавливаем запрос
        String category = "test";
        String query = "?category=" + category;

        // Отправляем запрос
        String response = restSteps.sendRequest(GET, JSON,
                EndPoint.EXPENSE.getValue() + query, null, BAD_REQUEST);

        // Проверка
        Assertions.assertEquals("Unsupported query parameter - [test]", response);
    }

    @Test
    @DisplayName("Проверка игнорирования неподдерживаемых query параметров")
    void getWithWrongQueryParametr() {
        // Подготавливаем запрос
        String category = "test";
        String query = "?test=" + category;

        // Отправляем запрос
        String response = restSteps.sendRequest(GET, JSON,
                EndPoint.EXPENSE.getValue() + query, null, OK);

        // Проверка
        Entity[] actualEntities = jsonConverter.fromJson(response, Entity[].class);
        Assertions.assertEquals(dbSteps.getExpensesFromDb().size(), actualEntities.length,
                "Кол-во сущностей в БД и ответе на запрос должно совпадать");
    }


    private static Stream<Arguments> getAllEntitiesWithSpecificCategoryArguments() {
        return Stream.of(
                Arguments.of(FOOD.name()),
                Arguments.of(HOME.name()),
                Arguments.of(TRANSPORT.name()),
                Arguments.of(ENTERTAINMENTS.name()),
                Arguments.of(MEDICINE.name()),
                Arguments.of(TRAVEL.name()),
                Arguments.of(OTHER.name()),
                Arguments.of(UNKNOWN.name()),
                Arguments.of("food")
        );
    }

}
