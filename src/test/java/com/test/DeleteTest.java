package com.test;

import com.test.adapter.HttpAdapter;
import com.test.model.Expense;
import com.test.teststeps.DbSteps;
import com.test.teststeps.RestSteps;
import com.test.variables.DockerConstants;
import com.test.variables.EndPoint;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.test.variables.DockerConstants.ACCOUNT_BOOK_IMAGE_NAME;
import static com.test.variables.RestConstants.ContentType.JSON;
import static com.test.variables.RestConstants.RequestMethod.DELETE;
import static com.test.variables.RestConstants.ResponseCode.NON_AUTHORITATIVE_INFORMATION;
import static com.test.variables.RestConstants.ResponseCode.OK;

@Slf4j
@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DeleteTest {
    private static RestSteps restSteps;
    private static DbSteps dbSteps;

    @Container
    private static final GenericContainer account_book =
            new GenericContainer(ACCOUNT_BOOK_IMAGE_NAME)
                    .withExposedPorts(DockerConstants.DEFAULT_HTTP_PORT)
                    .withLogConsumer(new Slf4jLogConsumer(log));

    @BeforeAll
    static void beforeAll() {
        String BOOK_HOST = account_book.getContainerIpAddress();
        Integer BOOK_PORT = account_book.getMappedPort(DockerConstants.DEFAULT_HTTP_PORT);
        HttpAdapter httpAdapter = HttpAdapter.builder()
                .uri("http://" + BOOK_HOST)
                .port(BOOK_PORT)
                .build();
        restSteps = new RestSteps(httpAdapter);
        dbSteps = new DbSteps(httpAdapter);
    }

    @BeforeEach
    void beforeEach() {
        dbSteps.generateTestValuesInDb();
    }

    @AfterEach
    void afterEach() {
        dbSteps.cleanUpDb();
    }

    @Test
    @Order(1)
    @DisplayName("Проверка удаления всех сущностей из БД")
    void deleteAllEntitiesRequest() {
        // Выполняем запрос
        // Если посмотреть, тест вполне нормальный. Но зная реализацию могу отметить тот факт, что из памяти не удалится
        // сущность с id=13. Тут не факт, что кто-то создаст достаточное кол-во тестовых данных. В качестве подсказки,
        // что что-то не так, код ответа изменён на 203.
        // Ниже есть тест на это - deleteAllEntitiesIfTheirMoreThan13
//        String response = restSteps.sendRequest(DELETE, JSON, EndPoint.EXPENSE.getValue(), null, OK);
        String response = restSteps.sendRequest(DELETE, JSON,
                EndPoint.EXPENSE.getValue(), null, NON_AUTHORITATIVE_INFORMATION);

        // Проверка
        Assertions.assertEquals("OK", response, "Должно быть ОК");
        Assertions.assertEquals(0, dbSteps.getExpensesFromDb().size(), "В БД не должно быть записей");
    }

    /**
     * Такой тест может не воспроизводиться, учитывая, что айдишники в тестируемом сервисе построены на атомике.
     * Но, так как тут используется Testcontainers, проблем нет. :)
     * P.S. Думаю стоит выпилить логику с 13 айдишником. Так как это специальное дополнение, в то время как остальные
     * "косяки" это результат "недосмотра".
     */
    @Order(2)
    @RepeatedTest(3)
    @DisplayName("Проверка удаления всех сущностей из БД v.2")
    void deleteAllEntitiesIfTheirMoreThan13() {
        // Подготавливаем проверочные данные
        for (int i = 0; i < 3; i++) {
            dbSteps.generateTestValuesInDb();
        }
        Map<String, Expense> entitiesFromDb = dbSteps.getExpensesFromDb();
        int expectedDbSize = entitiesFromDb.size();

        // Отправляем запрос
        String response = restSteps.sendRequest(DELETE, JSON,
                EndPoint.EXPENSE.getValue(), null, NON_AUTHORITATIVE_INFORMATION);

        // Проверка
        Assertions.assertEquals("OK", response, "Должно быть ОК");
        Assertions.assertEquals(1, dbSteps.getExpensesFromDb().size(), "В БД должна остаться только одна запись");
        Assertions.assertNotNull(dbSteps.getExpenseFromDb("13"), "В БД должна остаться запись с id=13");
    }

    @Test
    @Order(3)
    @DisplayName("Проверка удаления конкретной сущности из БД")
    void deleteCurrentEntityRequest() {
        // Подготавливаем проверочные данные
        Map<String, Expense> entitiesFromDb = dbSteps.getExpensesFromDb();
        int expectedDbSize = entitiesFromDb.size();

        // Выполняем запрос и проверяем состояние базы
        AtomicInteger removeCount = new AtomicInteger(1);
        entitiesFromDb.forEach((key, expectedExpense) -> {
            int bdSizeDifference = removeCount.getAndIncrement();
            String idPath = "/" + key;
            String response = restSteps.sendRequest(DELETE, JSON,
                    EndPoint.EXPENSE.getValue() + idPath, null, OK);
            Assertions.assertEquals("OK", response, "Должно быть ОК");

            Assertions.assertEquals(bdSizeDifference, expectedDbSize - dbSteps.getExpensesFromDb().size(),
                    "Сущностей в БД должно стать меньше");
            Assertions.assertNull(dbSteps.getExpenseFromDb(key), "В БД не должно быть сущности с id - " + key);
        });
    }

    @Test
    @Order(4)
    @DisplayName("Проверка удаления несуществующей сущности из БД")
    void deleteMissingIdRequest() {
        // Подготавливаем проверочные данные
        String id = "213";
        Map<String, Expense> entitiesFromDb = dbSteps.getExpensesFromDb();
        int expectedDbSize = entitiesFromDb.size();

        // Отправляем запрос
        // TODO Тоже можно обсудить логику. Мне кажется, что отвечать ОК на удаление того чего нет - это нормально.
        String response = restSteps.sendRequest(DELETE, JSON,
                EndPoint.EXPENSE.getValue() + "/" + id, null, OK);

        // Проверка
        Assertions.assertEquals("OK", response, "Всё должно быть ОК");
        Assertions.assertEquals(expectedDbSize, dbSteps.getExpensesFromDb().size(),
                "Кол-во сущностей в БД не должно измениться");
    }
}
