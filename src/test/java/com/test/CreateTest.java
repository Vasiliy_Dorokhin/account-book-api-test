package com.test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.adapter.HttpAdapter;
import com.test.model.Expense;
import com.test.test_object_builders.TestRequestCreator;
import com.test.teststeps.DbSteps;
import com.test.teststeps.RestSteps;
import com.test.variables.EndPoint;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.stream.Stream;

import static com.test.model.Expense.ExpenseCategory.ENTERTAINMENTS;
import static com.test.model.Expense.ExpenseCategory.FOOD;
import static com.test.model.Expense.ExpenseCategory.HOME;
import static com.test.model.Expense.ExpenseCategory.MEDICINE;
import static com.test.model.Expense.ExpenseCategory.OTHER;
import static com.test.model.Expense.ExpenseCategory.TRANSPORT;
import static com.test.model.Expense.ExpenseCategory.TRAVEL;
import static com.test.variables.DockerConstants.ACCOUNT_BOOK_IMAGE_NAME;
import static com.test.variables.DockerConstants.DEFAULT_HTTP_PORT;
import static com.test.variables.RestConstants.ContentType.JSON;
import static com.test.variables.RestConstants.RequestMethod.POST;
import static com.test.variables.RestConstants.ResponseCode.*;


@Slf4j
@Testcontainers
public class CreateTest {
    private static RestSteps restSteps;
    private static DbSteps dbSteps;
    private Gson jsonConverter = new GsonBuilder().create();

    @Container
    private static final GenericContainer account_book =
            new GenericContainer(ACCOUNT_BOOK_IMAGE_NAME)
                    .withExposedPorts(DEFAULT_HTTP_PORT)
            .withLogConsumer(new Slf4jLogConsumer(log));

    @BeforeAll
    static void beforeAll() {
        String BOOK_HOST = account_book.getContainerIpAddress();
        Integer BOOK_PORT = account_book.getMappedPort(DEFAULT_HTTP_PORT);
        HttpAdapter httpAdapter = HttpAdapter.builder()
                .uri("http://" + BOOK_HOST)
                .port(BOOK_PORT)
                .build();
        restSteps = new RestSteps(httpAdapter);
        dbSteps = new DbSteps(httpAdapter);
    }

    /**
     * Проверяем базовый кейс создания сущности.
     * Передаём различные кейсы по типу и имени.
     * Данные подобраны так, что без отображения определённых условий в документации, сущности должны создаваться.
     *
     * На это тесте находятятся косяки по переводу комментов в нижний регистр и что использование категории MEDICINE
     * приводит к отличному от ожидаемого результату
     */
    @ParameterizedTest(name = "[{index}] expense_category - [{0}], expense_comment - [{1}]")
    @MethodSource("correctRequestArguments")
    @DisplayName("Проверка записи расхода по корректному запросу")
    void correctRequest(Expense.ExpenseCategory category, String comment) {
        // Готовим тело зспроса
        Expense expectedExpense = Expense.builder()
                .category(category)
                .comment(comment)
                .build();
        String request = jsonConverter.toJson(expectedExpense);

        // Отправляем запрос
        String id = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue(), request, CREATED);

        // Проверка
        // Предполагается, что сущность будет создаваться в базе и здесь должен быть запрос в базу.
        // Так как "база" в памяти приложения, проверяем через GET запрос и имитацию базы. :)
        Expense actualExpense = dbSteps.getExpenseFromDb(id);
        Assertions.assertNotNull(actualExpense,
                "В базе должна присутствовать сущность с id полученным в ответе на запрос создания");
        // TODO: Имя переводится в lowercase
        // TODO: Расход с категорией MEDICINE создаётся в базе с параметрами -
        //  [{"expense_amount":0.0,"expense_category":"UNKNOWN","expense_comment":"empty_comment"}]
        expenseCheck(expectedExpense, actualExpense);
    }

    @Test
    @DisplayName("Проверка обработки неподдерживаемой категории")
    void unsupportedExpenseTypeValue() {
        // Готовим тело зспроса
        String request = TestRequestCreator.create("TestCategory", "TestComment");

        // Отправляем запрос
        // Здесь можно написать, что в подобной ситуации ожидается ошибка из разряда 4XX так как ошибка со стороны клиента.
        // TODO Стоит добавить валидацию.
        // Тут же стоит указать, что запись траты по категории UNKNOWN возвращает ошибку 404.
        // Поменял ожидаемый код, так как желательно проверить и сообщение
//        String response = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue(), request, BAD_REQUEST);
        String response = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue(), request, INTERNAL_SERVER_ERROR);

        // Проверка
        Assertions.assertEquals("<html><body><h2>500 Internal Server Error</h2></body></html>", response,
                "Будет норм обработка, будет норм сообщение");
    }

    /**
     * Раз уж есть числовые параметры, по хорошему должны быть тесты на граничные значения.
     * Как минимум случай отрицательных значений.
     * Но так как по реализации сумма зануляется при отрицательных значениях и значениях больше 500
     * Решил накидать параметризованный тест по этим цифрам, так же в данные добавил целочисленные значения
     */
    @MethodSource("amountLimitValuesArguments")
    @DisplayName("Проверка граничных значений возраста")
    @ParameterizedTest(name = "[{index}] expense_amount - [{0}]")
    void amountLimitValues(Number amount, Double expectedAmount) {
        // Готовим тело зспроса
        String request = TestRequestCreator.create(amount);

        // Отправляем запрос
        // TODO Вообще, тут конечно нужно обсуждать какое поведение правильное
        String id = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue(), request, CREATED);

        // Проверка
        Expense actualExpense = dbSteps.getExpenseFromDb(id);
        Assertions.assertNotNull(actualExpense,
                "В базе должна присутствовать сущность с id полученным в ответе на запрос создания");
        Assertions.assertEquals(expectedAmount, actualExpense.getAmount(), "По реализации сумма должна быть - " + expectedAmount);
    }

    @Test
    @DisplayName("Проверка типа значения поля суммы")
    void amountNotNumberValue() {
        // Готовим тело зспроса
        String ExpenseAmount = "test";
        String request = TestRequestCreator.create(ExpenseAmount, Expense.ExpenseCategory.FOOD.name(), "TestComment");

        // Отправляем запрос
        // TODO Ожидаемый ответ в таком случае должен быть с кодом 400, но отсутствует валидация типа, отсюда IllegalArgumentException и 500.
        //  Стоит добавить валидацию.
        // Поменял ожидаемый код, так как желательно проверить и сообщение
//        String response = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue(), request, BAD_REQUEST);
        String response = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue(), request, INTERNAL_SERVER_ERROR);

        // Проверка
        Assertions.assertEquals("<html><body><h2>500 Internal Server Error</h2></body></html>", response,
                "Будет норм обработка, будет норм сообщение");
    }

    @ParameterizedTest(name = "[{index}] null поле - [{1}], сериализуется - [{2}]")
    @MethodSource("requiredFieldsAbsentOrNullArguments")
    @DisplayName("Проверка валидации обязательных полей")
    void requiredFieldsAbsentOrNull(Expense expense, String fieldName, boolean serializeNulls) {
        String request;
        Gson converterWithNulls = new GsonBuilder().serializeNulls().create();
        if (serializeNulls) {
            request = converterWithNulls.toJson(expense);
        } else {
            request = jsonConverter.toJson(expense);
        }

        // Отправляем запрос
        //  TODO Ожидем 400, в случае отсутствия поля получаем 200 с ошибкой и ничего не создаётся.
        //   Если поле имеет значение null возвращается 500
        //   Стоит добавить валидацию и бла-бла-бла.
//        String id = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue(), request, BAD_REQUEST);
        HttpAdapter.Response response = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue(), request);

        // Проверка
        String expectedMessageFor200 = "Error. Pls fill all parameters";
        String expectedMessageFor500 = "<html><body><h2>500 Internal Server Error</h2></body></html>";
        if (response.responseCodeIsExpected(OK)) {
            Assertions.assertTrue(response.responseBodyIsExpected(expectedMessageFor200),
                    "Можно смело указывать поле которого не хватило");
        } else {
            Assertions.assertTrue(response.responseBodyIsExpected(expectedMessageFor500),
                    "Можно смело указывать поле которого не хватило");
        }
    }

    @Test
    @DisplayName("Проверка обработки запроса на создание по ошибочному пути")
    void createRequestOnRandomId() {
        // Готовим тело зспроса
        String idPath = "/213";
        String request = TestRequestCreator.create("TestComment");

        // Отправляем запрос
        // TODO Ожидем 405, получаем 404. Тут можно обсуждать,
        //  казалось бы что по относитльно валидному пути можно кинуть ошибку по методу, а не самому пути
        // Поменял ожидаемый код, так как желательно проверить и сообщение
//        String response = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue() + idPath, request, METHOD_NOT_ALLOWED);
        String response = restSteps.sendRequest(POST, JSON, EndPoint.EXPENSE.getValue() + idPath, request, NOT_FOUND);

        // Проверка
        Assertions.assertEquals("<html><body><h2>404 Not found</h2></body></html>", response,
                "Будет норм обработка, будет норм сообщение");
    }

    private void expenseCheck(Expense expectedExpense, Expense actualExpense) {
        Assertions.assertEquals(expectedExpense.getAmount(), actualExpense.getAmount(),
                "Сумма должна совпадать с указанной при создании");
        Assertions.assertEquals(expectedExpense.getCategory(), actualExpense.getCategory(),
                "Категория должна совпадать с указанной при создании");
        Assertions.assertEquals(expectedExpense.getComment(), actualExpense.getComment(),
                "Коментарий должен совпадать с указанным при создании");
    }


    private static Stream<Arguments> correctRequestArguments() {
        return Stream.of(
                Arguments.of(FOOD, "Шава"),
                Arguments.of(HOME, " новая люстра "),
                Arguments.of(TRANSPORT, "маршрутка №黒#$2'"),
                Arguments.of(ENTERTAINMENTS, ""),
                Arguments.of(MEDICINE, "Аспирин"),
                Arguments.of(TRAVEL, "Eurotrip"),
                Arguments.of(OTHER, "")
        );
    }

    private static Stream<Arguments> amountLimitValuesArguments() {
        return Stream.of(
                Arguments.of(-1, 0.0),
                Arguments.of(-0.1, 0.0),
                Arguments.of(0, 0.0),
                Arguments.of(1, 1.0),
                Arguments.of(500,500.0),
                Arguments.of(500.1, 0.0)
        );
    }

    private static Stream<Arguments> requiredFieldsAbsentOrNullArguments() {
        Expense withoutAmount = Expense.builder().amount(null).build();
        Expense withoutCategory = Expense.builder().category(null).build();
        Expense withoutComment = Expense.builder().comment(null).build();
        return Stream.of(
                Arguments.of(withoutAmount, "expense_amount", true),
                Arguments.of(withoutAmount, "expense_amount", false),
                Arguments.of(withoutCategory, "expense_category", true),
                Arguments.of(withoutCategory, "expense_category", false),
                Arguments.of(withoutComment, "expense_comment", true),
                Arguments.of(withoutComment, "expense_comment", false)
        );
    }
}
