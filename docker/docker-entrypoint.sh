#!/bin/sh
set -e

if [[ "$1" = 'run' ]]; then
    cd /app
    exec /usr/bin/java -Dfile.encoding=utf-8 \
         -XX:+UseContainerSupport \
         -Xmx${MAX_HEAP_SIZE_GB}g \
         -jar account-book.jar
fi

exec "$@"
